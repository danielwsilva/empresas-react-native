![N|Solid](https://img.shields.io/badge/feito%20por-danielwsilva-%237519C1?style=for-the-badge&logo=github)

## 💻 Sobre o projeto

A aplicação tem a finalidade de listar as empresas e visualizar seus respectivos detalhes para os usuários logados.

[Telas da Aplicação (Figma)](https://bitbucket.org/danielwsilva/empresas-react-native/src/master/src/assets/figma.png)

[Gif da Aplicação em funcionamento](https://bitbucket.org/danielwsilva/empresas-react-native/src/master/src/assets/ioasys.gif)

## 🧭 Rodando a aplicação

```bash

# Clone este repositório
$ git clone https://danielwsilva@bitbucket.org/danielwsilva/empresas-react-native.git

# Acesse a pasta do projeto no seu terminal/cmd
$ cd empresas-react-native

# Instale as dependências
$ yarn

# Execute a aplicação em modo de desenvolvimento
$ yarn start

```

## 🛠 Tecnologias

Bibliotecas envolvidas na construção do projeto:

#### React Native - Bibliotecas

- **react-navigation** - Navegação
- **styled-components** - CSS in JS
- **async-storage** - Persistência de dados
- **axios** - Requisições API
- **expo-google-fonts** - Importação de fonts
- **lottie-react-native** - Animação
- **expo-linear-gradient** - Gradiente
- **react-native-gesture-handler** - Componentes com comportamento Nativo
- **react-native-iphone-x-helper** - Ajuda a projetar quando o smartphone tem entalhos
- **react-native-responsive-fontsize** - Fontes responsivas


## 🎁 Bônus

Não consegui implementar os bônus, pois ainda não tenho conhecimento.

## 📝 Observações

Conforme conversa inicial com a Brenda, foi mencionado que eu não tinha experiência profissional com React Native. Mediante a isto, apliquei todo o meu conhecimento adquirido em projetos pessoas e estudos. Atualmente adquiri o Bootcamp Ignite (Rocketseat) para continuar evoluindo.